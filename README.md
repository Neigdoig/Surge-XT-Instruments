# Surge-XT-Instruments

This is a repository of instruments I had made using Surge XT. All of them are licensed under CC-BY-4.0, so I'd like some credit if you do use any of these instruments. That way, people can know that I made them, and can use them for themselves.